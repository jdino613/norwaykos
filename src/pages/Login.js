import React from "react";
import LoginForm from "components/forms/LoginForm";
import { Redirect } from "react-router-dom";
import ApiErrors from "components/forms/ApiErrors";
import { withAuth } from "providers/AuthProvider";

import DefaultNavbar from "components/shared/DefaultNavbar.js";
import TransparentFooter from "components/shared/TransparentFooter.js";

// reactstrap components
import { Card, CardHeader, CardBody, Form, Container, Col } from "reactstrap";
import video from "assets/login.mp4";

class Login extends React.Component {
  state = {
    shouldRedirect: false,
    errors: [],
  };

  signIn = (loginData) => {
    this.props.auth
      .signIn(loginData)
      .then((_) => this.setState({ shouldRedirect: true }))
      .catch((errors) => this.setState({ errors }));
  };

  render() {
    const { errors, shouldRedirect } = this.state;
    // const { message } = this.props.location.state || "";

    if (shouldRedirect) {
      return <Redirect to={{ pathname: "/" }} />;
    }

    return (
      <>
        <DefaultNavbar />
        <div className="page-header clear-filter" filter-color="blue">
          <div className="page-header-image">
            <video
              className="videoTag"
              autoPlay
              loop
              muted
              style={{
                minHeight: "100%",
                minWidth: "100%",
                position: "fixed",
                bottom: "0",
                right: "0",
              }}
            >
              <source src={video} type="video/mp4" />
            </video>
          </div>

          <div className="content">
            <Container>
              <Col className="ml-auto mr-auto" md="4">
                <Card className="card-login card-plain">
                  <CardHeader className="text-center">
                    <div className="logo-container">
                      <img
                        alt="..."
                        src={require("assets/img/kos-white.png")}
                      ></img>
                    </div>
                  </CardHeader>
                  <CardBody>
                    <Form className="form">
                      <LoginForm onSubmit={this.signIn} />
                      <ApiErrors errors={errors} />
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Container>
          </div>
          <TransparentFooter />
        </div>
      </>
    );
  }
}

export default withAuth(Login);
