import React from "react";
import RentalForm from "components/forms/RentalForm";
import { createRental } from "actions";
import { Redirect } from "react-router-dom";
import Header from "components/shared/Header";

class RentalNew extends React.Component {
  state = {
    shouldRedirect: false,
  };

  handleRentalCreate = (rentalData) => {
    createRental(rentalData)
      .then((_) => this.setState({ shouldRedirect: true }))
      .catch((_) => console.log("Errors"));
  };

  render() {
    const { shouldRedirect } = this.state;

    if (shouldRedirect) {
      return <Redirect to={{ pathname: "/" }} />;
    }

    return (
      <>
        <Header />
        <div className="container kos-container" style={{ marginTop: "-20px" }}>
          <section id="newRental">
            <div className="kos-form">
              <div className="row">
                <div className="col-md-5">
                  <h1 className="page-title">Create Rental</h1>
                  <RentalForm onSubmit={this.handleRentalCreate} />
                  {/* <div>
                <p>
                  Some Errors
                </p>
              </div> */}
                </div>
                <div className="col-md-6 ml-auto">
                  <div className="image-container">
                    <h2 className="catchphrase">
                      Come join us in showcasing true Norwegian hospitality!
                    </h2>
                    {/* <img src="" alt="" /> */}
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </>
    );
  }
}

export default RentalNew;
