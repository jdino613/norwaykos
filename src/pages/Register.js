import React from "react";
import RegisterForm from "components/forms/RegisterForm";
import { registerUser } from "actions";
import { Redirect } from "react-router-dom";
import ApiErrors from "components/forms/ApiErrors";

// reactstrap components
import {
  Card,
  CardBody,
  CardTitle,
  Form,
  Container,
  Row,
  Col,
} from "reactstrap";

// core components
import DefaultNavbar from "components/shared/DefaultNavbar.js";
import TransparentFooter from "components/shared/TransparentFooter.js";

class Register extends React.Component {
  state = {
    shouldRedirect: false,
    errors: [],
  };

  signUp = (registerData) => {
    registerUser(registerData)
      .then(() => this.setState({ shouldRedirect: true }))
      .catch((errors) => this.setState({ errors }));
  };

  render() {
    const { shouldRedirect, errors } = this.state;

    if (shouldRedirect) {
      return (
        <Redirect
          to={{
            pathname: "/login",
            state: { message: "You have been succesfuly registered!" },
          }}
        />
      );
    }
    return (
      <>
        <DefaultNavbar />
        <div className="page-header header-filter" filter-color="black">
          <div
            className="page-header-image"
            style={{
              backgroundImage: "url(" + require("assets/img/igloo2.jpg") + ")",
            }}
          ></div>

          <div className="content" style={{ marginTop: "8%" }}>
            <Container>
              <Row>
                <Col className="ml-auto mr-auto" md="6" lg="6">
                  <div className="info info-horizontal text-left">
                    <div className="description" style={{ marginTop: "25%" }}>
                      <h1 className="info-title">
                        KOS is your destination for delight
                      </h1>
                      <h3 className="description" style={{ fontWeight: "400" }}>
                        Book confidently. Stay comfortably.
                      </h3>
                    </div>
                  </div>
                </Col>
                <Col className="mr-auto" md="6" lg="4">
                  <Card
                    className="card-signup"
                    style={{ marginBottom: "10px" }}
                  >
                    <CardBody>
                      <CardTitle className="text-center" tag="h4">
                        Register
                      </CardTitle>

                      <h2 className="card-description">Register</h2>

                      <Form>
                        <RegisterForm onSubmit={this.signUp} />
                        <ApiErrors errors={errors} />
                      </Form>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Container>
          </div>
          <TransparentFooter />
        </div>
      </>
    );
  }
}

export default Register;
