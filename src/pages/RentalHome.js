import React from "react";
import RentalCard from "components/rental/RentalCard";
import { connect } from "react-redux";
import { fetchRentals } from "actions";
import RentalSearchInput from "components/rental/RentalSearchInput";
import Header from "components/shared/Header";
import DefaultFooter from "components/shared/DefaultFooter.js";

class RentalHome extends React.Component {
  componentDidMount() {
    this.props.dispatch(fetchRentals());
  }

  renderRentals = (rentals) =>
    rentals.map((rental) => (
      <div key={rental._id} className="col-md-4">
        <RentalCard rental={rental} />
      </div>
    ));

  render() {
    const { rentals } = this.props;

    return (
      <>
        <Header />
        <div className="container kos-container" style={{ marginTop: "-20px" }}>
          <div className="card-list">
            <h1 className="page-title">
              Comfort of a home, confidence of a hotel!
            </h1>
            <div style={{ paddingBottom: "45px", marginRight: "25rem" }}>
              <RentalSearchInput />
            </div>
            <div className="row">{this.renderRentals(rentals)}</div>
          </div>
        </div>
        <DefaultFooter />
      </>
    );
  }
}

const mapStateToProps = ({ rentals }) => {
  return {
    rentals: rentals.items,
  };
};

export default connect(mapStateToProps)(RentalHome);
