import React, { useEffect } from "react";

// reactstrap components
import {
  Button,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col,
} from "reactstrap";

// core components
import DefaultNavbar from "components/shared/DefaultNavbar.js";

import LandingPageHeader from "components/shared/LandingPageHeader.js";
import DarkFooter from "components/shared/DarkFooter.js";
import { useAuth } from "providers/AuthProvider";

function LandingPage() {
  const authService = useAuth();

  useEffect(() => {
    authService.checkAuthState();
  }, [authService]);

  const [firstFocus, setFirstFocus] = React.useState(false);
  const [lastFocus, setLastFocus] = React.useState(false);

  React.useEffect(() => {
    document.body.classList.add("landing-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    return function cleanup() {
      document.body.classList.remove("landing-page");
      document.body.classList.remove("sidebar-collapse");
    };
  });
  return (
    <>
      <DefaultNavbar logout={authService.signOut} />

      <div className="wrapper">
        <LandingPageHeader
          color="transparent"
          brand="KOS"
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white",
          }}
        />

        <div className="section section-about-us">
          <Container>
            <Row>
              <Col className="ml-auto mr-auto text-center" md="8">
                <h2 className="title">Who we are?</h2>
                <h5 className="description" style={{ color: "#242424" }}>
                  We are elevating the way people travel with our curated
                  end-to-end experience. Driven by technology, data, and human
                  connections every step of the way, KOS is crafting the most
                  delightful stay for all of our guests.
                </h5>
              </Col>
            </Row>
            <div className="separator separator-primary"></div>
            <div className="section-story-overview">
              <Row>
                <Col md="6">
                  <div
                    className="image-container image-left"
                    style={{
                      backgroundImage:
                        "url(" + require("assets/img/fjord2.jpeg") + ")",
                    }}
                  >
                    <p className="blockquote blockquote-info">
                      "KOS curates; as opposed to larger accommodation
                      platforms, each property is handpicked and styled to
                      maintain a level of quality and brand." <br></br>
                      <br></br>
                      <small>
                        -Forbes,{" "}
                        <span style={{ fontStyle: "italic" }}>2019</span>
                      </small>
                    </p>
                  </div>
                  <div
                    className="image-container"
                    style={{
                      marginLeft: "1px",
                      backgroundImage:
                        "url(" + require("assets/img/bergen.jpeg") + ")",
                    }}
                  ></div>
                </Col>
                <Col md="5">
                  <div
                    className="image-container image-right"
                    style={{
                      backgroundImage:
                        "url(" + require("assets/img/tromso.jpeg") + ")",
                    }}
                  ></div>
                  <h3>So what does KOS mean?</h3>
                  <p style={{ color: "#242424" }}>
                    The most important word in the Norwegian language consists
                    of only three letters, but in return, it’s glowing with
                    warmth, kindness, caring, togetherness, and laughter.{" "}
                    <span style={{ fontWeight: "600" }}>
                      ‘Kos’ is Norwegian for having a good time.
                    </span>{" "}
                    It is the kind of instant happiness you get when you feel
                    safe, warm, and good together. Norway's mighty nature and
                    distinct changes of seasons make people gather together to
                    create intimate moments of coziness.
                  </p>
                  <p style={{ color: "#242424" }}>
                    The surrounding environment is one of the cleanest you’ll
                    find anywhere in the world. The water is so pure that you
                    can quench your thirst straight from a stream running down
                    the falls or the lake into which it flows, and the air is so
                    fresh that you can feel it deep down in your lungs. Up here,
                    winters are cold and summers comfortably warm. All in all,
                    what you’ll find here is perfection.
                  </p>
                  <p>
                    Norway is one of the safest countries in the world, so KOS
                    is an easy and risk-free choice for a successful holiday.
                    And our international staff are here to serve you in a
                    variety of languages.
                  </p>
                </Col>
              </Row>
            </div>
          </Container>
        </div>

        <div className="section section-contact-us text-center">
          <Container>
            <h2 className="title">Book with us</h2>
            <p className="description" style={{ color: "#242424" }}>
              Every year millions of people visit Norway and we welcome every
              one of you. "Velkommen" as we say here in Norway. Hopefully this
              website will answer your questions. If you haven't found what you
              were looking for please feel free to contact us.
            </p>
            <Row>
              <Col
                style={{ color: "#242424" }}
                className="text-center ml-auto mr-auto"
                lg="6"
                md="8"
              >
                <InputGroup
                  className={
                    "input-lg" + (firstFocus ? " input-group-focus" : "")
                  }
                >
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="now-ui-icons users_circle-08"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="Full Name..."
                    type="text"
                    onFocus={() => setFirstFocus(true)}
                    onBlur={() => setFirstFocus(false)}
                  ></Input>
                </InputGroup>
                <InputGroup
                  className={
                    "input-lg" + (lastFocus ? " input-group-focus" : "")
                  }
                >
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="now-ui-icons ui-1_email-85"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="Email..."
                    type="text"
                    onFocus={() => setLastFocus(true)}
                    onBlur={() => setLastFocus(false)}
                  ></Input>
                </InputGroup>
                <div className="textarea-container">
                  <Input
                    cols="80"
                    name="name"
                    placeholder="Type a message..."
                    rows="4"
                    type="textarea"
                  ></Input>
                </div>
                <div className="send-button">
                  <Button
                    block
                    className="btn-round"
                    color="info"
                    href="#pablo"
                    onClick={(e) => e.preventDefault()}
                    size="lg"
                  >
                    Send Message
                  </Button>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        <DarkFooter />
      </div>
    </>
  );
}

export default LandingPage;
