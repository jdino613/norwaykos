import React from "react";
import BookingListing from "components/booking/BookingListing";
import { connect } from "react-redux";
import { fetchUserBookings, deleteBooking } from "actions";
import Header from "components/shared/Header";

class ManageBookings extends React.Component {
  componentDidMount() {
    this.props.dispatch(fetchUserBookings());
  }

  deleteBooking = (bookingId) => {
    const canDelete = this.askForPermission();
    if (!canDelete) {
      return;
    }

    this.props.dispatch(deleteBooking(bookingId));
  };

  askForPermission() {
    return window.confirm("Are you sure you want to delete this booking?");
  }

  render() {
    const { bookings, errors, isFetching } = this.props;
    return (
      <>
        <Header />
        <div className="container kos-container" style={{ marginTop: "-20px" }}>
          <BookingListing
            errors={errors}
            isFetching={isFetching}
            title="My Bookings"
            bookings={bookings}
            renderMenu={(bookingId) => (
              <button
                onClick={() => this.deleteBooking(bookingId)}
                className="btn btn-danger"
              >
                Delete
              </button>
            )}
          />
        </div>
      </>
    );
  }
}

const mapStateToProps = ({ manage }) => {
  return {
    bookings: manage.bookings.items,
    isFetching: manage.bookings.isFetching,
    errors: manage.bookings.errors,
  };
};

export default connect(mapStateToProps)(ManageBookings);
