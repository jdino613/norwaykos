import React from "react";
// reactstrap components
import { Container, Row, Col } from "reactstrap";

// core components
import DefaultNavbar from "components/shared/DefaultNavbar.js";
import CovidHeader from "components/shared/CovidHeader.js";
import DefaultFooter from "components/shared/DefaultFooter.js";

function Covid() {
  return (
    <>
      <DefaultNavbar />
      <div className="wrapper">
        <CovidHeader />
        <div className="section">
          <div className="about-description text-center">
            <div className="features-3">
              <Container>
                <Row>
                  <Col>
                    <h5
                      className="title text-left"
                      style={{ marginBottom: "30px" }}
                    >
                      Norwegian government has closed the borders for all but
                      Norwegian citizens and foreigners that possess a residence
                      or work permit. Everybody that doesn’t have a residence
                      permit or work permit must leave the country. The
                      government also discourage all non-essential domestic
                      travel.
                    </h5>
                    <ul className="text-left">
                      <li>
                        The government has closed the border to foreign
                        nationals who lack a residence or work permit in Norway.
                        They will be turned away at the border under provisions
                        of a Norwegian law relating to the control of
                        communicable diseases. The main airports will stay open
                        so that foreigners and tourists travelling in Norway can
                        return home.
                      </li>
                      <li>
                        In addition, temporary entry and exit controls will be
                        introduced at the internal Schengen border.
                      </li>
                      <li>
                        Travellers not residing in Norway or having a work
                        permit are instructed to leave the country as soon as
                        possible.
                      </li>
                      <li>
                        Norwegian main airports are not closing. All Norwegian
                        citizens and persons who live or work in Norway will
                        continue to be let into the country. Exemptions will
                        therefore be provided for European Economic Area (EEA)
                        citizens and their family members who reside in Norway.
                        Exemptions are also being prepared for EEA citizens who
                        work in Norway.
                      </li>
                      <li>
                        People with Norwegian passports or residence/work
                        permits who have arrived in Norway from another country
                        must stay in quarantine at home for 14 days after
                        arrival, regardless of whether or not they have
                        symptoms. If they have symptoms, they must be isolated
                        immediately.{" "}
                      </li>
                      <li>
                        All travellers coming from abroad must undergo
                        quarantine if they wish to stay in Norway. One provision
                        in the regulations provides that persons who travel in
                        connection with work between their home and workplace,
                        and in so doing cross the borders between Norway, Sweden
                        and Finland, are exempt from the duty of quarantine.
                      </li>
                      <li>
                        Transit passengers: People free of illness and symptoms
                        may fly to their final destination to be quarantined in
                        their own home. During the travel, they must keep
                        distance to others if possible.
                      </li>
                      <li>
                        Check local and regional quarantine regulations if you
                        have to travel inside of Norway, as many places are now
                        quarantining people from other parts of the country.
                        Avoid all kinds of travel that are not strictly
                        necessary, both domestic and abroad.
                      </li>
                      <li>
                        Norwegians abroad must comply with the recommendations
                        of the local authorities. Questions related to refund
                        rights etc. must be directed to your insurance company.
                      </li>
                      <li>Avoid public transport if you can.</li>
                      <li>
                        Avoid other places where you can easily get close to
                        others.
                      </li>
                    </ul>
                    <h4>
                      If you have questions about the coronavirus (COVID-19)
                      while travelling in Norway, please call the national
                      information telephone at (+47) 815 55 015.
                    </h4>
                    {/* <h4 className="description">
                      For founder Janica, travel is about bringing people
                      together. She noticed that traveling with friends and
                      family often meant being separated into different rooms or
                      neighborhoods. In 2020, she started Kos and set out to
                      solve these problems by providing larger spaces that focus
                      on comfort, consistency, and world-class design.
                    </h4> */}
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
        </div>
        <DefaultFooter />
      </div>
    </>
  );
}

export default Covid;
