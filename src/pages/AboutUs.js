import React, { useEffect } from "react";
import { useAuth } from "providers/AuthProvider";

// reactstrap components
import { Container, Row, Col } from "reactstrap";

// core components
import DefaultNavbar from "components/shared/DefaultNavbar.js";
import AboutUsHeader from "components/shared/AboutUsHeader.js";
import DefaultFooter from "components/shared/DefaultFooter.js";

function AboutUs() {
  const authService = useAuth();

  useEffect(() => {
    authService.checkAuthState();
  }, [authService]);

  React.useEffect(() => {
    document.body.classList.add("about-us");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    return function cleanup() {
      document.body.classList.remove("about-us");
      document.body.classList.remove("sidebar-collapse");
    };
  });
  return (
    <>
      <DefaultNavbar logout={authService.signOut} />
      <div className="wrapper">
        <AboutUsHeader />
        <div className="section">
          <div className="about-description text-center">
            <div className="features-3">
              <Container>
                <Row>
                  <Col className="mr-auto ml-auto" md="8">
                    <h2 className="title">
                      CONNECTING the world through travel
                    </h2>
                    <h4 className="description">
                      For founder Janica, travel is about bringing people
                      together. She noticed that traveling with friends and
                      family often meant being separated into different rooms or
                      neighborhoods. In 2020, she started KOS and set out to
                      solve these problems by providing larger spaces that focus
                      on comfort, consistency, and world-class design.
                    </h4>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
          <div className="separator-line bg-info"></div>
          <div className="projects-5">
            <Container>
              <Row>
                <Col className="ml-auto mr-auto text-center" md="8">
                  <h2 className="title">LOOK HOW FAR we've come</h2>
                  <h4 className="description">
                    Today, the KOS team comprises talent from top companies in
                    hospitality, real estate, technology, and design. We take
                    pride in making each stay feel as unique & immersive as the
                    destination itself. By investing in technology that takes
                    the friction out of travel, KOS seamlessly connects millions
                    of travelers to memorable experiences, a variety of
                    transportation options, and incredible places to stay – from
                    homes to hotels, and much more.
                  </h4>
                  <div className="section-space"></div>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="about-team team-4"></div>
        </div>
        <DefaultFooter />
      </div>
    </>
  );
}

export default AboutUs;
