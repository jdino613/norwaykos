import axiosService from "services/AxiosService";
import { extractApiErrors, deleteResource } from "./index";
const { kosAxios } = axiosService;

export const createBooking = (booking) => {
  return kosAxios
    .post("/bookings", booking)
    .then((res) => res.data)
    .catch((err) => Promise.reject(extractApiErrors(err.response || {})));
};

export const getBookings = (rentalId) => {
  return kosAxios.get(`/bookings?rental=${rentalId}`).then((res) => res.data);
};

export const fetchUserBookings = () => (dispatch) => {
  dispatch({ type: "REQUEST_DATA", resource: "manage-bookings" });
  return kosAxios
    .get("/bookings/me")
    .then((res) => res.data)
    .then((bookings) => {
      dispatch({
        type: "REQUEST_DATA_COMPLETE",
        data: bookings,
        resource: "manage-bookings",
      });
    });
};

export const fetchReceivedBookings = () => (dispatch) => {
  dispatch({ type: "REQUEST_DATA", resource: "received-bookings" });
  return kosAxios
    .get("/bookings/received")
    .then((res) => res.data)
    .then((bookings) => {
      dispatch({
        type: "REQUEST_DATA_COMPLETE",
        data: bookings,
        resource: "received-bookings",
      });
    });
};

export const deleteBooking = (bookingId) => (dispatch) => {
  return dispatch(
    deleteResource({
      url: `/bookings/${bookingId}`,
      resource: "manage-bookings",
    })
  );
};
