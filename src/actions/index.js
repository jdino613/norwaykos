import axiosService from "services/AxiosService";
const { kosAxios } = axiosService;

export const uploadImage = (image) => {
  const formData = new FormData();
  formData.append("image", image);

  return kosAxios.post("/image-upload", formData).then((res) => res.data);
};

export const extractApiErrors = (resError) => {
  let errors = [{ title: "Error!", detail: "Ooops, something went wrong!" }];

  if (resError && resError.data && resError.data.errors) {
    errors = resError.data.errors;
  }

  return errors;
};

export const deleteResource = ({ url, resource }) => (dispatch) => {
  return kosAxios
    .delete(url)
    .then((res) => res.data)
    .then(({ id }) => {
      dispatch({
        type: "DELETE_RESOURCE",
        id,
        resource,
      });
    })
    .catch((error) => {
      dispatch({
        type: "REQUEST_ERROR",
        errors: extractApiErrors(error.response || []),
        resource,
      });
    });
};

export * from "./auth";
export * from "./rentals";
export * from "./bookings";

// Payments
export const getPendingPayments = () => {
  return kosAxios
    .get("/payments")
    .then((res) => res.data)
    .catch(({ response }) => Promise.reject(response.data.errors));
};

export const acceptPayment = (payment) => {
  return kosAxios
    .post("/payments/accept", payment)
    .then((res) => res.data)
    .catch(({ response }) => Promise.reject(response.data.errors));
};

export const declinePayment = (payment) => {
  return kosAxios
    .post("/payments/decline", payment)
    .then((res) => res.data)
    .catch(({ response }) => Promise.reject(response.data.errors));
};
