import React from "react";
import { useForm } from "react-hook-form";
import { sameAs } from "helpers/validators";

import { Button, CardFooter } from "reactstrap";

// eslint-disable-next-line
const EMAIL_PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const RegisterForm = ({ onSubmit }) => {
  React.useEffect(() => {
    document.body.classList.add("signup-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    return function cleanup() {
      document.body.classList.remove("signup-page");
      document.body.classList.remove("sidebar-collapse");
    };
  });
  const { register, errors, handleSubmit, getValues } = useForm();
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="form-group">
        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="now-ui-icons users_circle-08"></i>
            </span>
          </div>
          <input
            ref={register({ required: true })}
            type="text"
            className="form-control"
            name="username"
            id="username"
            placeholder="Name"
          />
          {errors.username && (
            <div className="alert alert-danger">
              {errors.username.type === "required" && (
                <span>Username is required!</span>
              )}
            </div>
          )}
        </div>
      </div>

      <div className="form-group">
        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="now-ui-icons ui-1_email-85"></i>
            </span>
          </div>
          <input
            ref={register({ required: true, pattern: EMAIL_PATTERN })}
            type="email"
            className="form-control"
            name="email"
            id="email"
            placeholder="Your Email"
          />
          {errors.email && (
            <div className="alert alert-danger">
              {errors.email.type === "required" && (
                <span>Email is required!</span>
              )}
              {errors.email.type === "pattern" && (
                <span>Not valid email format!</span>
              )}
            </div>
          )}
        </div>
      </div>

      <div className="form-group">
        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="now-ui-icons ui-1_lock-circle-open"></i>
            </span>
          </div>
          <input
            ref={register({ required: true, minLength: 6 })}
            type="password"
            className="form-control"
            name="password"
            id="password"
            placeholder="Password"
          />
          {errors.password && (
            <div className="alert alert-danger">
              {errors.password.type === "required" && (
                <span>Password is required!</span>
              )}
              {errors.password.type === "minLength" && (
                <span>Minimum length of password is 6 characters!</span>
              )}
            </div>
          )}
        </div>
      </div>

      <div className="form-group">
        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="now-ui-icons ui-1_lock-circle-open"></i>
            </span>
          </div>
          <input
            ref={register({
              required: true,
              minLength: 6,
              validate: { sameAs: sameAs("password", getValues) },
            })}
            type="password"
            className="form-control"
            name="passwordConfirmation"
            id="passwordConfirmation"
            placeholder="Confirm Password"
          />
          {errors.passwordConfirmation && (
            <div className="alert alert-danger">
              {errors.passwordConfirmation.type === "required" && (
                <span>Password confirmation is required!</span>
              )}
              {errors.passwordConfirmation.type === "minLength" && (
                <span>
                  Minimum length of password confirmation is 6 characters!
                </span>
              )}
              {errors.passwordConfirmation.type === "sameAs" && (
                <span>
                  Password confirmation has to be the same as password!
                </span>
              )}
            </div>
          )}
        </div>
      </div>
      <CardFooter className="text-center">
        <Button className="btn-round" color="info" type="submit" size="lg">
          Get Started
        </Button>
      </CardFooter>
    </form>
  );
};

export default RegisterForm;
