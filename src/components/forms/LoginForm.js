import React from "react";
import { useForm, ErrorMessage } from "react-hook-form";
import FormError from "./FormError";

// reactstrap components
import { Button, CardFooter } from "reactstrap";

const Error = ({ children }) => (
  <div className="alert alert-danger">{children}</div>
);

// eslint-disable-next-line
const EMAIL_PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const LoginForm = ({ onSubmit }) => {
  React.useEffect(() => {
    document.body.classList.add("login-page");
    document.body.classList.add("sidebar-collapse");
    document.documentElement.classList.remove("nav-open");
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    return function cleanup() {
      document.body.classList.remove("login-page");
      document.body.classList.remove("sidebar-collapse");
    };
  });

  const { register, handleSubmit, errors } = useForm();
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="form-group">
        <div className="input-group mb-3" style={{ height: "3rem" }}>
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="now-ui-icons users_circle-08"></i>
            </span>
          </div>
          <input
            ref={register({
              required: "Email is required",
              pattern: {
                value: EMAIL_PATTERN,
                message: "Invalid email format!",
              },
            })}
            name="email"
            type="email"
            className="form-control"
            id="email"
            placeholder="Email"
          />
        </div>
        <FormError errors={errors} name="email">
          {(message) => <p>{message}</p>}
        </FormError>
      </div>

      <div className="form-group">
        <div className="input-group mb-3" style={{ height: "3rem" }}>
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="now-ui-icons ui-1_lock-circle-open"></i>
            </span>
          </div>
          <input
            ref={register({
              required: "Password is required!",
              minLength: {
                value: 6,
                message: "Minimum length of password is 6 characters!",
              },
            })}
            name="password"
            type="password"
            className="form-control"
            id="password"
            placeholder="Password"
          />
        </div>
        <ErrorMessage as={<Error />} errors={errors} name="password">
          {({ message }) => <p>{message}</p>}
        </ErrorMessage>
      </div>
      <CardFooter className="text-center">
        <Button
          type="submit"
          block
          className="btn-round"
          color="info"
          size="lg"
        >
          Get Started
        </Button>

        <div className="pull-left">
          <h6>
            <a className="link" href="/register">
              Create Account
            </a>
          </h6>
        </div>
        <div className="pull-right">
          <h6>
            <a className="link" href="/register">
              Need Help?
            </a>
          </h6>
        </div>
      </CardFooter>
    </form>
  );
};

export default LoginForm;
