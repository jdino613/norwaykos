import React, { useState } from "react";
import { useHistory } from "react-router-dom";

const RentalSearchInput = () => {
  const [location, setLocation] = useState("");
  const history = useHistory();

  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      handleSearch();
    }
  };

  const handleSearch = () => {
    location ? history.push(`/rentals/${location}/homes`) : history.push("/");
  };

  return (
    <div className="form-inline-sm d-flex justify-content-center mx-8">
      <input
        onKeyPress={handleKeyPress}
        onChange={(e) => setLocation(e.target.value)}
        value={location}
        className="form-control kos-search m-6"
        type="search"
        placeholder="Search City"
      />
      &nbsp;
      <button
        onClick={handleSearch}
        className="btn btn-kos-main my-2 my-sm-0 btn-primary"
        type="button"
      >
        Search
      </button>
    </div>
  );
};

export default RentalSearchInput;
