import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const RentalAssets = () => (
  <div className="rental-assets">
    <h3 className="title">Amenities</h3>
    <div className="row">
      <div className="col-md-6 assets">
        <ul style={{ listStyle: "none" }}>
          <li className="py-1">
            <span>
              <FontAwesomeIcon icon="asterisk" /> Cooling
            </span>
          </li>
          <li className="py-1">
            <span>
              <FontAwesomeIcon icon="thermometer" /> Heating
            </span>
          </li>
          <li className="py-1">
            <span>
              <FontAwesomeIcon icon="location-arrow" /> Iron
            </span>
          </li>
        </ul>
      </div>
      <div className="col-md-6 assets">
        <ul style={{ listStyle: "none" }}>
          <li className="py-1">
            <span>
              <FontAwesomeIcon icon="desktop" /> Working area
            </span>
          </li>
          <li className="py-1">
            <span>
              <FontAwesomeIcon icon="cube" /> Washing machine
            </span>
          </li>
          <li className="py-1">
            <span>
              <FontAwesomeIcon icon="archive" /> Dishwasher
            </span>
          </li>
        </ul>
      </div>
    </div>
  </div>
);

export default RentalAssets;
