import React from "react";

// reactstrap components
import { Button, Container, Alert } from "reactstrap";

// core components

function LandingPageHeader() {
  let pageHeader = React.createRef();

  const [alert2, setAlert2] = React.useState(true);

  React.useEffect(() => {
    if (window.innerWidth > 991) {
      const updateScroll = () => {
        let windowScrollTop = window.pageYOffset / 3;
        pageHeader.current.style.transform =
          "translate3d(0," + windowScrollTop + "px,0)";
      };
      window.addEventListener("scroll", updateScroll);
      return function cleanup() {
        window.removeEventListener("scroll", updateScroll);
      };
    }
  });
  return (
    <>
      <div className="page-header page-header-base">
        <div
          className="page-header-image clear-filter"
          style={{
            backgroundImage: "url(" + require("assets/img/lofoten.jpeg") + ")",
          }}
          ref={pageHeader}
        ></div>
        <div
          style={{
            position: "fixed",
            zIndex: "100000",
            top: "12%",
            width: "100%",
          }}
        >
          <Alert color="info" isOpen={alert2}>
            <Container>
              <div className="alert-icon">
                <i className="now-ui-icons travel_info"></i>
              </div>
              <strong>TRAVEL ALERT!</strong>&nbsp; &nbsp;
              <a
                href="/covid"
                target="_blank"
                style={{ color: "white", textDecoration: "underline" }}
              >
                Important information about the Coronavirus situation in Norway
              </a>
              {/* <Button style={{ margin: "0" }}>More Info</Button> */}
              <button
                type="button"
                className="close"
                onClick={() => setAlert2(false)}
              >
                <span aria-hidden="true">
                  <i className="now-ui-icons ui-1_simple-remove"></i>
                </span>
              </button>
            </Container>
          </Alert>
        </div>
        <div
          className="content-center"
          style={{
            top: "55%",
            left: "35%",
            float: "left",
            marginLeft: "50px",
            paddingRight: "24%",
          }}
        >
          <Container>
            <h1 className="title text-left">Discover Norway</h1>
            <h4 className="text-left">
              The real Norwegian experience is far more than any one city or
              location. Enhance a trip with KOS with the opportunity to further
              discover the wonders of Norway. We are here to help you uncover
              all that Norway has to offer.
            </h4>
            {/* <br /> */}
            <div style={{ float: "left" }}>
              <Button
                color="danger"
                size="lg"
                href="https://www.youtube.com/watch?v=_nE8AhurAs0"
                target="_blank"
                rel="noopener noreferrer"
              >
                <i className="fas fa-play" />
                Watch video
              </Button>
            </div>
          </Container>
        </div>
      </div>
    </>
  );
}

export default LandingPageHeader;
