/*eslint-disable*/
import React from "react";

// reactstrap components
import { Container } from "reactstrap";

function TransparentFooter() {
  return (
    <footer className="footer">
      <Container>
        <nav>
          <ul>
            <li>
              <a href="https://gitlab.com/jdino613" target="_blank">
                Follow Us
              </a>
            </li>
            <li>
              <a href="/about">About Us</a>
            </li>
          </ul>
        </nav>
        <div className="copyright" id="copyright">
          © {new Date().getFullYear()} KOS, Inc. All rights reserved.
        </div>
      </Container>
    </footer>
  );
}

export default TransparentFooter;
