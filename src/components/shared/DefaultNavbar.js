import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { useAuth } from "providers/AuthProvider";

// reactstrap components
import {
  Collapse,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
} from "reactstrap";

const DefaultNavbar = ({ username, isAuth, logout }) => {
  const authService = useAuth();

  useEffect(() => {
    authService.checkAuthState();
  }, [authService]);

  const [navbarColor, setNavbarColor] = React.useState("navbar-transparent");
  const [collapseOpen, setCollapseOpen] = React.useState(false);
  React.useEffect(() => {
    const updateNavbarColor = () => {
      if (
        document.documentElement.scrollTop > 399 ||
        document.body.scrollTop > 399
      ) {
        setNavbarColor("");
      } else if (
        document.documentElement.scrollTop < 400 ||
        document.body.scrollTop < 400
      ) {
        setNavbarColor("navbar-transparent");
      }
    };
    document.body.classList.add("sidebar-collapse");
    window.addEventListener("scroll", updateNavbarColor);
    return function cleanup() {
      window.removeEventListener("scroll", updateNavbarColor);
      document.body.classList.remove("sidebar-collapse");
    };
  });
  return (
    <>
      {collapseOpen ? (
        <div
          id="bodyClick"
          onClick={() => {
            document.documentElement.classList.toggle("nav-open");
            setCollapseOpen(false);
          }}
        />
      ) : null}
      <Navbar
        className={"fixed-top " + navbarColor}
        color="warning"
        expand="lg"
      >
        <Container>
          <div className="navbar-translate" style={{ fontSize: "30px" }}>
            <NavbarBrand href="/landing-page" id="navbar-brand">
              <span>
                <img
                  alt=""
                  style={{ height: "30px" }}
                  src={require("assets/img/kos-icon-red.png")}
                />
              </span>{" "}
              KOS
            </NavbarBrand>
            <button
              className="navbar-toggler navbar-toggler"
              onClick={() => {
                document.documentElement.classList.toggle("nav-open");
                setCollapseOpen(!collapseOpen);
              }}
              aria-expanded={collapseOpen}
              type="button"
            >
              <span className="navbar-toggler-bar top-bar"></span>
              <span className="navbar-toggler-bar middle-bar"></span>
              <span className="navbar-toggler-bar bottom-bar"></span>
            </button>
          </div>
          <Collapse
            className="justify-content-end"
            isOpen={collapseOpen}
            navbar
          >
            <Nav navbar style={{ fontSize: "1.2rem" }}>
              {isAuth && (
                <li>
                  <div className="nav-link">Welcome {username}</div>
                </li>
              )}
              <NavItem>
                <NavLink to="/about" tag={Link}>
                  About
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="/" tag={Link}>
                  Book Now
                </NavLink>
              </NavItem>
              {/* <NavItem>
                <NavLink to="/login" tag={Link}>
                  Login
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/register">Register</NavLink>
              </NavItem> */}

              {isAuth && (
                <>
                  <li className="nav-item dropdown">
                    <a
                      className="nav-link dropdown-toggle"
                      href="manage"
                      id="navbarDropdown"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      Manage
                    </a>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="navbarDropdown"
                    >
                      <Link className="dropdown-item" to="/rentals/new">
                        New Rental
                      </Link>
                      <Link className="dropdown-item" to="/rentals/manage">
                        Manage Rentals
                      </Link>
                      <Link className="dropdown-item" to="/bookings/manage">
                        Manage Bookings
                      </Link>
                      <Link className="dropdown-item" to="/bookings/received">
                        Received Bookings
                      </Link>
                    </div>
                  </li>
                  <li className="nav-item">
                    <Link
                      onClick={authService.signOut}
                      className="nav-link"
                      // style={{ cursor: "pointer" }}
                    >
                      Logout
                    </Link>
                  </li>
                </>
              )}

              {!isAuth && (
                <>
                  <li className="nav-item">
                    <Link className="nav-link" to="/login">
                      Login
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/register">
                      Register
                    </Link>
                  </li>
                </>
              )}
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </>
  );
};

const mapStateToProps = ({ auth: { username, isAuth } }) => {
  return {
    username,
    isAuth,
  };
};

export default connect(mapStateToProps)(DefaultNavbar);
