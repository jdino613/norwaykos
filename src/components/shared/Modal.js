import React, { useState } from "react";
import { Modal } from "react-responsive-modal";
import "index.css";
import "react-responsive-modal/styles.css";

const KosModal = ({
  title = "Modal Window",
  subtitle = "Confirm your data",
  openBtn: OpenBtn,
  onSubmit,
  children,
}) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      {!OpenBtn && (
        <button onClick={() => setIsOpen(true)} className="btn btn-success">
          Open
        </button>
      )}
      {OpenBtn && <div onClick={() => setIsOpen(true)}>{OpenBtn}</div>}
      <Modal
        focusTrapped={false}
        open={isOpen}
        onClose={() => setIsOpen(false)}
        classNames={{ modal: "kos-modal" }}
      >
        <h4 className="modal-title title">{title}</h4>
        <p className="modal-subtitle">{subtitle}</p>
        <div className="modal-body">{children}</div>
        <div className="modal-footer">
          <button
            onClick={() => onSubmit(() => setIsOpen(false))}
            type="button"
            className="btn btn-success"
          >
            Confirm
          </button>
          <button
            onClick={() => setIsOpen(false)}
            type="button"
            className="btn btn-danger"
          >
            Cancel
          </button>
        </div>
      </Modal>
    </>
  );
};

export default KosModal;
