const mongoose = require("mongoose");

const user1Id = mongoose.Types.ObjectId();
const user2Id = mongoose.Types.ObjectId();

// const image1Id = mongoose.Types.ObjectId();
// const image2Id = mongoose.Types.ObjectId();
// const image3Id = mongoose.Types.ObjectId();
// const image4Id = mongoose.Types.ObjectId();
// const image5Id = mongoose.Types.ObjectId();
// const image6Id = mongoose.Types.ObjectId();
// const image7Id = mongoose.Types.ObjectId();
// const image8Id = mongoose.Types.ObjectId();
// const image9Id = mongoose.Types.ObjectId();

// exports.images = [
//   {
//     _id: image1Id,
//     cloudinaryId: "3eefdec9-daee-4025-a48a-b6305f17961d_zjom7n",
//     url:
//       "https://res.cloudinary.com/jlimpin613/image/upload/v1588603572/3eefdec9-daee-4025-a48a-b6305f17961d_zjom7n.jpg",
//   },
//   {
//     _id: image2Id,
//     cloudinaryId: "5dbb9e04-cb7a-488d-9261-241b4fdfddf4_p6ybfa",
//     url:
//       "https://res.cloudinary.com/jlimpin613/image/upload/v1588603586/5dbb9e04-cb7a-488d-9261-241b4fdfddf4_p6ybfa.jpg",
//   },
//   {
//     _id: image3Id,
//     cloudinaryId: "glassIgloo3_tfcgep",
//     url:
//       "https://res.cloudinary.com/jlimpin613/image/upload/v1588603595/glassIgloo3_tfcgep.jpg",
//   },
//   {
//     _id: image4Id,
//     cloudinaryId: "1491593441918_hlyqfa",
//     url:
//       "https://res.cloudinary.com/jlimpin613/image/upload/v1588603557/1491593441918_hlyqfa.jpg",
//   },
//   {
//     _id: image5Id,
//     cloudinaryId: "1970677_zczyzp",
//     url:
//       "https://res.cloudinary.com/jlimpin613/image/upload/v1588603628/1970677_zczyzp.jpg",
//   },
//   {
//     _id: image6Id,
//     cloudinaryId: "IMG_3607.jpg_wcbz5r",
//     url:
//       "https://res.cloudinary.com/jlimpin613/image/upload/v1588603609/IMG_3607.jpg_wcbz5r.webp",
//   },
//   {
//     _id: image7Id,
//     cloudinaryId: "5092_tdfosu",
//     url:
//       "https://res.cloudinary.com/jlimpin613/image/upload/v1588604283/5092_tdfosu.jpg",
//   },
//   {
//     _id: image8Id,
//     cloudinaryId: "Svinoya_Rorbuer_96412_s0vtkn",
//     url:
//       "https://res.cloudinary.com/jlimpin613/image/upload/v1588604293/Svinoya_Rorbuer_96412_s0vtkn.jpg",
//   },
//   {
//     _id: image9Id,
//     cloudinaryId: "1973078_jgpb5d",
//     url:
//       "https://res.cloudinary.com/jlimpin613/image/upload/v1588604311/1973078_jgpb5d.jpg",
//   },
// ];

exports.users = [
  {
    _id: user1Id,
    username: "Bob Ross",
    email: "bob@bob.com",
    password: "123456",
  },
  {
    _id: user2Id,
    username: "Kygo",
    email: "kygo@gmail.com",
    password: "123456",
  },
];

exports.rentals = [
  {
    title: "Aurora Cabin",
    city: "kvalsund",
    street: "hammerfest",
    category: "cabin",
    image:
      "https://a0.muscache.com/im/pictures/3eefdec9-daee-4025-a48a-b6305f17961d.jpg?aki_policy=xx_large",
    numOfRooms: 3,
    shared: false,
    description: "A stop before North Cape!",
    dailyPrice: 29,
    // owner: user1Id,
  },
  {
    title: "Room for two in a new flat",
    city: "tromsø",
    street: "kvaløya",
    category: "apartment",
    image:
      "https://a0.muscache.com/im/pictures/5dbb9e04-cb7a-488d-9261-241b4fdfddf4.jpg?aki_policy=xx_large",
    numOfRooms: 1,
    shared: true,
    description:
      "The location is in a quiet housing area and with a splendid view!",
    dailyPrice: 22,
    // owner: user1Id,
  },
  {
    title: "Small Glass Igloo",
    city: "rotsund",
    street: "Spåkenesveien 281",
    category: "igloo",
    image:
      "http://80.75.103.15/NetReservationsKakslauttanen/Images/GlassIgloo/glassIgloo3.jpg",
    numOfRooms: 5,
    shared: false,
    description:
      "Comforting warmth of our cosy log chalets with the sensational views of the Artic sky.",
    dailyPrice: 497,
    // owner: user2Id,
  },
  {
    title: "Bjornfjell Mountain Lodge",
    city: "alta",
    street: "Nordelvdalen 170",
    category: "cabin",
    image:
      "https://travel.home.sndimg.com/content/dam/images/travel/fullrights/2016/10/19/CI_TripAdvisor_Vacation_Rentals-Fairbanks-Alaska-Northern-Lights.jpg.rend.hgtvcom.616.462.suffix/1491593441918.jpeg",
    numOfRooms: 1,
    shared: false,
    description: "A taste of serenity, and a dash of adventurous complexion",
    dailyPrice: 236,
    // owner: user2Id,
  },
  {
    title: "Scandic Kirkenes",
    city: "bjørnevatn",
    street: "Sandnesdalen 14",
    category: "lodge",
    image:
      "https://img.theculturetrip.com/1440x960/smart/wp-content/uploads/2018/03/1970677.jpg",
    numOfRooms: 1,
    shared: false,
    description: "Welcome to Winter Wonderland!",
    dailyPrice: 272,
    // _id: user1Id,
  },
  {
    title: "Lyngen North",
    city: "rotsund",
    street: "Spåkenesveien 281",
    category: "igloo",
    image:
      "https://www.heartmybackpack.com/wp-content/uploads/2018/07/IMG_3607.jpg.webp",
    numOfRooms: 1,
    shared: false,
    description:
      "Relaxing atmosphere and spectacular views towards the mighty Lyngen Alps.",
    dailyPrice: 227,
    // _id: user1Id,
  },
  {
    title: "Rorbu",
    city: "svolvær",
    street: "Gunnar Bergs vei 2",
    category: "cabin",
    image:
      "https://i.guim.co.uk/img/media/abe43d0799d6ea9dd6ad05b01803aa9c51941054/0_322_5092_3057/master/5092.jpg?width=780&quality=45&auto=format&fit=max&dpr=2&s=d3f281bf1796f5eb85aebcd3eca17202",
    numOfRooms: 1,
    shared: false,
    description:
      "This is absolutely the best location to stay in Lofoten. The view from the living room does not seem real.",
    dailyPrice: 128,
    // _id: user1Id,
  },
  {
    title: "Vestfjord Suites",
    city: "svolvær",
    street: "Gunnar Bergs vei 2",
    category: "cabin",
    image:
      "https://d19lgisewk9l6l.cloudfront.net/assetbank/Svinoya_Rorbuer_96412.jpg",
    numOfRooms: 1,
    shared: false,
    description: "Beautiful location between the mountains and the sea.",
    dailyPrice: 336,
    // _id: user1Id,
  },
  {
    title: "Stranda Booking",
    city: "stranda",
    street: "Strandveien 17",
    category: "cottage",
    image:
      "https://img.theculturetrip.com/1440x960/smart/wp-content/uploads/2018/03/1973078.jpg",
    numOfRooms: 1,
    shared: false,
    description:
      "Canoeing, skiing, fishing and golfing to keep you occupied – you know, in case just relaxing and taking in the magnificent views is not enough.",
    dailyPrice: 290,
    // _id: user1Id,
  },
];
