// {
//   _id: "2137129312",
//   city: "Kvalsund",
//   title: "Aurora Cabin",
// },
// {
//   _id: "asjkdajdasnda",
//   city: "Tromsø",
//   title: "Room for two in a new flat",
// },
// {
//   _id: "3",
//   city: "rotsund",
//   title: "Small Glass Igloo"
// }

const Rental = require("../models/rental");
const Booking = require("../models/booking");

// get all rentals
exports.getRentals = async (req, res) => {
  const { city } = req.query;

  const query = city ? { city: city.toLowerCase() } : {};

  try {
    const rentals = await Rental.find(query);
    return res.json(rentals);
  } catch (error) {
    return res.mongoError(error);
  }
};

// Get user rentals
exports.getUserRentals = async (req, res) => {
  const { user } = res.locals;

  try {
    const rentals = await Rental.find({ owner: user });
    return res.json(rentals);
  } catch (error) {
    return res.mongoError(error);
  }
};

// Get
exports.getRentalById = async (req, res) => {
  const { rentalId } = req.params;

  try {
    const rental = await Rental.findById(rentalId).populate(
      "owner",
      "-password -_id"
    );
    return res.json(rental);
  } catch (error) {
    return res.mongoError(error);
  }
};

// Verify
exports.verifyUser = async (req, res) => {
  const { user } = res.locals;
  const { rentalId } = req.params;

  try {
    const rental = await Rental.findById(rentalId).populate("owner");

    if (rental.owner.id !== user.id) {
      return res.sendApiError({
        title: "Invalid User",
        detail: "You are not owner of this rental!",
      });
    }

    return res.json({ status: "verified" });
  } catch (error) {
    return res.mongoError(error);
  }
};

// Create
exports.createRental = (req, res) => {
  const rentalData = req.body;
  rentalData.owner = res.locals.user;

  Rental.create(rentalData, (error, createdRental) => {
    if (error) {
      return res.mongoError(error);
    }

    return res.json(createdRental);
  });
};

// Update
// exports.updateRental = async (req, res) => {
//   const { rentalId } = req.params;
//   const { user } = res.locals;
//   const rentalData = req.body;

//   try {
//     const rental = await Rental.findById(rentalId).populate("owner");

//     if (rental.owner.id !== user.id) {
//       return res.sendApiError({
//         title: "Invalid User",
//         detail: "You are not owner of this rental!",
//       });
//     }

//     rental.set(rentalData);
//     await rental.save();
//     return res.status(200).send(rental);
//   } catch (error) {
//     return res.mongoError(error);
//   }
// };

exports.updateRental = async (req, res) => {
  const { rentalId } = req.params;
  const { user } = res.locals;
  const rentalData = req.body;

  try {
    const rental = await Rental.findById(rentalId).populate("owner");

    if (rental.owner.id !== user.id) {
      return res.sendApiError({
        title: "Invalid User",
        detail: "You are not owner of this rental!",
      });
    }

    rental.set(rentalData);
    await rental.save();
    return res.status(200).send(rental);
  } catch (error) {
    return res.mongoError(error);
  }
};

// Delete
// exports.deleteRental = async (req, res) => {
//   const { rentalId } = req.params;
//   const { user } = res.locals;

//   try {
//     const rental = await Rental.findById(rentalId).populate("owner");
//     const bookings = await Booking.find({ rental });

//     if (user.id !== rental.owner.id) {
//       return res.sendApiError({
//         title: "Invalid User",
//         detail: "You are not owner of this rental!",
//       });
//     }

//     if (bookings && bookings.length > 0) {
//       return res.sendApiError({
//         title: "Active Bookings",
//         detail: "Cannot delete rental with active booking!",
//       });
//     }

//     await rental.remove();
//     return res.json({ id: rentalId });
//   } catch (error) {
//     return res.mongoError(error);
//   }
// };

exports.deleteRental = async (req, res) => {
  const { rentalId } = req.params;
  const { user } = res.locals;

  try {
    const rental = await Rental.findById(rentalId).populate("owner");
    const bookings = await Booking.find({ rental });

    if (user.id !== rental.owner.id) {
      return res.sendApiError({
        title: "Invalid User",
        detail: "You are not owner of this rental!",
      });
    }

    if (bookings && bookings.length > 0) {
      return res.sendApiError({
        title: "Active Bookings",
        detail: "Cannot delete rental with active booking!",
      });
    }

    await rental.remove();
    return res.json({ id: rentalId });
  } catch (error) {
    return res.mongoError(error);
  }
};

// middlewares
// exports.isUserRentalOwner = (req, res, next) => {
//   const { rental } = req.body;
//   const user = res.locals.user;

//   Rental.findById(rental)
//     .populate("owner")
//     .exec((error, foundRental) => {
//       if (error) {
//         return res.mongoError(error);
//       }

//       if (foundRental.owner.id === user.id) {
//         return res.sendApiError({
//           title: "Invalid User",
//           detail: "Cannot create booking on your rental",
//         });
//       }

//       next();
//     });
// };
exports.isUserRentalOwner = (req, res, next) => {
  const { rental } = req.body;
  const user = res.locals.user;

  if (!rental) {
    return res.sendApiError({
      title: "Booking Error",
      detail: "Cannot create booking to undefined rental",
    });
  }

  Rental.findById(rental)
    .populate("owner")
    .exec((error, foundRental) => {
      if (error) {
        return res.mongoError(error);
      }

      if (foundRental.owner.id === user.id) {
        return res.sendApiError({
          title: "Invalid User",
          detail: "Cannot create booking on your rental",
        });
      }

      next();
    });
};
