// const express = require("express");
// const router = express.Router();

// const UserCtrl = require("../controllers/user");
// const PaymentCtrl = require("../controllers/payment");

// router.get("", UserCtrl.authMiddleware, PaymentCtrl.getPendingPayments);

// router.post("/accept", UserCtrl.authMiddleware, PaymentCtrl.confirmPayment);
// router.post("/decline", UserCtrl.authMiddleware, PaymentCtrl.declinePayment);

// module.exports = router;

const express = require("express");

const router = express.Router();

const stripe = require("stripe")("sk_test_r2NeaSDULDjApNI4DJ0yuija00zetLJuHn");
const BookingModel = require("../models/booking");

router.post("/charge", async (req, res) => {
  try {
    const customer = await stripe.customers.create({
      email: req.body.email,
      description: "Payment for Booking",
      source: "tok_visa",
    });

    let chargeResponse = await stripe.charges.create({
      amount: req.body.amount,
      currency: "php",
      source: "tok_visa",
      description: "Payment for Booking",
    });

    await BookingModel.findByIdAndUpdate(req.body.id, {
      status: "Paid",
      payment: "Stripe",
    });

    res.send(chargeResponse);
  } catch (e) {
    console.log(e);
    res.status(422).send("Bad Request. Please try again.");
  }
});

module.exports = router;
