// const express = require("express");
// const router = express.Router();
// const {
//   getRentals,
//   getRentalById,
//   createRental,
//   getUserRentals,
//   deleteRental,
//   updateRental,
//   verifyUser,
// } = require("../controllers/rentals");
// const { onlyAuthUser } = require("../controllers/users");

// router.get("/", getRentals);
// router.get("/me", onlyAuthUser, getUserRentals);
// router.get("/:rentalId", getRentalById);
// router.get("/:rentalId/verify-user", onlyAuthUser, verifyUser);
// router.post("/", onlyAuthUser, createRental);
// router.patch("/:rentalId", onlyAuthUser, updateRental);
// router.delete("/:rentalId", onlyAuthUser, deleteRental);

// module.exports = router;

const express = require("express");
const router = express.Router();
const { onlyAuthUser } = require("../controllers/users");
const {
  getRentals,
  getRentalById,
  createRental,
  getUserRentals,
  deleteRental,
  updateRental,
  verifyUser,
} = require("../controllers/rentals");

// /api/v1/rentals?city=""
router.get("", getRentals);
router.get("/me", onlyAuthUser, getUserRentals);
router.get("/:rentalId", getRentalById);
router.get("/:rentalId/verify-user", onlyAuthUser, verifyUser);

router.post("", onlyAuthUser, createRental);

router.patch("/:rentalId", onlyAuthUser, updateRental);

router.delete("/:rentalId", onlyAuthUser, deleteRental);

module.exports = router;
