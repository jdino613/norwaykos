// const express = require("express");
// const router = express.Router();
// const {
//   createBooking,
//   getBookings,
//   getUserBookings,
//   getReceivedBookings,
//   deleteBooking,
// } = require("../controllers/bookings");
// const { onlyAuthUser } = require("../controllers/users");
// const { isUserRentalOwner } = require("../controllers/rentals");

// router.get("/me", onlyAuthUser, getUserBookings);
// router.post("/", onlyAuthUser, isUserRentalOwner, createBooking);

// router.delete("/:bookingId", onlyAuthUser, deleteBooking);

// module.exports = router;

const express = require("express");
const router = express.Router();
const {
  createBooking,
  getBookings,
  getUserBookings,
  getReceivedBookings,
  deleteBooking,
} = require("../controllers/bookings");
const { isUserRentalOwner } = require("../controllers/rentals");
const { onlyAuthUser } = require("../controllers/users");

// /api/v1/bookings?rental=""
router.get("", getBookings);
router.get("/received", onlyAuthUser, getReceivedBookings);
router.get("/me", onlyAuthUser, getUserBookings);
router.post("", onlyAuthUser, isUserRentalOwner, createBooking);

// DELETE: /api/v1/bookings/

router.delete("/:bookingId", onlyAuthUser, deleteBooking);

module.exports = router;
