exports.provideErrorHandler = (req, res, next) => {
  res.sendApiError = (config) => {
    const { status = 401, title, detail } = config;
    return res.status(status).send({ errors: [{ title, detail }] });
  };

  res.mongoError = (dbError) => {
    const normalizedErrors = [];
    const errorField = "errors";

    if (
      dbError &&
      dbError.hasOwnProperty(errorField) &&
      dbError.name === "ValidationError"
    ) {
      const errors = dbError[errorField];
      for (let property in errors) {
        normalizedErrors.push({
          title: property,
          detail: errors[property].message,
        });
      }
    } else {
      normalizedErrors.push({
        title: "Database Error",
        detail: "Bad Request. Please Try Again.",
      });
    }

    return res.status(401).send({ errors: normalizedErrors });
  };

  next();
};
